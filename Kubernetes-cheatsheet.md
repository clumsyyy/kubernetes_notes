

## Useful command aliases
```bash
alias k=kubectl
alias kdr='kubectl --dry-run=client -o yaml'

kdr create deployment mydeployment --image=nginx:latest > mydeployment.yaml
kdr run mynewpod --image=nginx:latest > mynewpod.yaml
```
## Create and generate templates
```bash
# pod
kubectl run mynewpod --image=nginx:latest --labels type=web --dry-run=client -o yaml > mynewpod.yaml

# pod service
kubectl expose pod mynewpod --port=80 --name=mynewpod-svc --type=NodePort --dry-run=client -o yaml > mynewpod-svc.yaml

# nodeport service
kubectl create service nodeport mypod --tcp=80:80 --node-port=30001 --dry-run=client -o yaml > mypod-service.yaml

# deployment
kubectl create deployment mynewdeployment --image=nginx:latest --dry-run=client -o yaml > mynewdeployment.yaml

# deployment service
kubectl expose deployment mydeployment --type=NodePort --port=8080 --name=mydeployment-service --dry-run=client -o yaml > mydeployment-service.yaml

# job
kubectl create job myjob --image=nginx:latest --dry-run=client -o yaml

# cron job
kubectl create cj mycronjob --image=nginx:latest --schedule="* * * * *"
--dry-run=client -o yaml
```