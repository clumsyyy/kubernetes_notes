1. Upgrade control planes first
2. Upgrade worker nodes second


## Control plane upgrade
1. Import target version of kubernetes containers needed to run a control plane if there is not internet
	- kube-controller-manager
	- kube-apiserver
	- kube-scheduler
	- kube-proxy
	- etcd
	- pause
	- coredns
2. Upgrade the kubeadm tool to the desired version
3. Run ```kubeadm upgrade plan``` to check that the update should work
4. Backup manifest files and kubelet config
	- /etc/kubernetes/manifests/ yaml files
	- /var/lib/kubelet/config.yaml
5. Run ```kubeadm upgrade apply <version>``` to upgrade the control plane
6. Update kubelet and kubectl to the correct version and restart the kubelet service


## Worker node upgrade
1. From a control plane node run ```kubectl drain <node> --ignore-daemonsets``` to remove pods
2. On the worker node upgrade all the kube tools (kubeadm, kubelet, kubectl) to the correct version
3. Restart the kubelet service
4. On the control plane run ```kubectl uncordon <node>``` to mark it as working again
5. Confirm on the control plane with ```kubectl get nodes -Aowide```

