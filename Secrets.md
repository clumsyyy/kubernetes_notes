Secret yaml files must contain base64 encoded values. Use this command to generate a file with the values encoded and ready to be applied.

```bash
kubectl create secret generic <name> --dry-run=client -o yaml --from-literal MY_KEY=sometexthere --from-literal MY_PASSWORD=sometexthere
```