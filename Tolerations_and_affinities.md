
##### tolerations for running on tainted hosts
```yaml
template:
spec:
	tolerations:
		- key: <name>
		  value: <item>
		  effect: <NoSchedule>
		  operator: Equal
```

affinities for preferring certain hosts

```yaml
template:
spec:
    affinity:
        nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
                nodeSelectorTerms:
                - matchExpressions:
                  - key: disktype
                    operator: In
                    values:
                    - ssd
```

