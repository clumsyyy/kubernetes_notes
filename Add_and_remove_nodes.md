
## Add
```bash
# Generate the join command using the command below
# Copy and paste the output to join a worker to a cluster
kubeadm token create --print-join-command

# Show tokens available
kubeadm token list
```

## Remove
```bash
# From control plane
kubectl drain <node-name> --ignore-daemonsets --delete-local-data
kubectl delete node <node-name>
```

